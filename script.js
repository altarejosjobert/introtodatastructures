console.log("Hello!")

console.log("This is intro to Linked List!")


/*let n1 = {
	data:100
}

let n2 = {
	data:200
}

n1.next = n2;
console.log(n1) */
/*{data: 100, next: {…}}data: 100next: {data: 200}__proto__: Object*/

class Node {
	// re: class https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes
	constructor(data,next = null){
		//The constructor method is a special method for creating and initializing an object created within a class
		this.data = data;
		this.next = next;
	}
}

/*const a1 = new Node(100);
console.log(a1); //Node {data: 100, next: null}*/

class linkedList {
	constructor(){
		this.head = null;
		this.size = 0;
	}

	//insert first node
	insertFirst(data){
		this.head = new Node (data, this.head);
		//this insert the data to the 1st/head

		this.size++;
	}

	//insert last node
	insertLast(data){
		let node = new Node(data);
		let current;

		//if empty, make this the head
		if(!this.head){
			this.head = node
		}else{
			current = this.head;
			while(current.next){
				current = current.next;
			}
			current.next = node;
		}

		this.size++;
	}

	//insert at index
	insertAt(data, index){
		if(typeof index === "number" && index !== undefined){
				if (index<0) {
					return console.log("Please input positive numbers only!")
				}else if(index>0 && index>this.size){
					//check if index input is out of range
					return console.log("Index out of range!");
				}else if (index === 0) {
					//index=0 means it will be the head
					this.head = new Node(data, this.head); //insertFirst(data)
					return;
				}else{
					const node = new Node(data);
					let current, previous;
					//set current to first
					current = this.head;
					let count = 0;
					while(count < index){
						previous = current;//node before index
						count++;
						current = current.next;//node after index
					}
					node.next = current;
					previous.next = node;
					this.size++;
				}
			}else{
				return console.log("Input a value for index and it must be a number.")
			}
	}

	//get at index
	getAt(index){
		let current = this.head;
		let count = 0;

		while(current){
			if (count == index) {
				console.log(current.data)
			}
			count++;
			current = current.next;
		}
		return null;
	}

	//remove at index
	removeAt(index){
		if (index > 0 && index > this.size) {
			//if out of range
			return;
		}
		let current = this.head;
		let previous;
		let count = 0;

		//if index = 0
		if (index === 0) {
			this.head = current.next;
		}else{
			while(count<index){
				count++;
				previous = current;
				current = current.next;
			}
			previous.next = current.next;
		}
		this.size--;//decrement this.size
	}

	//clear list
	clearList(){
		this.head = null;
		this.size = 0;
		return console.log("List has been cleared!")
	}

	//print list data
	printListData(){
		let current = this.head;
		while(current){
			console.log(current.data);
			current = current.next
		}
	}
}

const LL = new linkedList();
LL.insertFirst(100);
/*console.log(LL) 
linkedList {head: Node, size: 0}
head: Node {data: 100, next: null}
size: 0
__proto__: Object*/
LL.insertFirst(200);
/*console.log(LL); head: Node
data: 200
next: Node {data: 100, next: null}
__proto__:
constructor: class Node
__proto__: Object ETC ETC*/

LL.insertFirst(300);
//LL.printListData();
/*300 head/1st data due to insertFirst()
  200
  100*/

LL.insertLast(400);
//LL.printListData();
/*300
  200
  100
  400*/

LL.insertAt(500,2);
//LL.printListData();
/*300
  200
  500
  100
  400*/

//LL.getAt(3);//100
LL.removeAt(2);
//LL.printListData();
/*300
  200
  100
  400*/

LL.clearList();
LL.printListData();




